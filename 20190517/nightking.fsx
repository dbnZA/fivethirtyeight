#load "lib/simulation.fs"
open Simulation

let p d l =
    let rec p' n p =
        match n with
        | 0 -> p |> Seq.last
        | _ ->
            p
                |> Seq.skip 2
                |> Seq.append [1.0]
                |> Seq.mapFold (fun cum prev ->
                    let p = (0.5 * cum + 0.5 * prev)
                    p, p) 1.0
                |> fst
                |> p' (n - 1)
    p' l (Seq.init (d + l + 1) (fun _ -> 0.0))

let rec battle d l rnd =
    match (d, l) with
    | (0, _) -> true
    | (_, 0) -> false
    | (d, l) ->
        rnd |> if rnd |> Seq.head < 0.5 then battle (d + 1) (l - 1) else battle (d - 1) l

let seek l =
    let rec seek' d (dUp, pUp) (dDown, pDown) =
        let p = p d l
        if p = 0.5 then
            d
        elif dUp - dDown <= 2 then
            [abs (0.5 - p), d; 0.5 - pUp, dUp; pDown - 0.5, dDown]
                |> List.minBy fst
                |> snd
        elif p < 0.5 then
            seek' ((d - dDown) / 2 + dDown) (d, p) (dDown, pDown)
        else
            seek' ((dUp - d) / 2 + d) (dUp, pUp) (d, p)
    match l with
    | 1 -> 1
    | _ -> seek' (l / 2) (l, (p l l)) (0, 1.0)

printf "|  d |   l |     p  |\n"
printf "|---:|----:|-------:|\n"
for l in 1..7 do
    let d = seek l
    let p = p d l
    battle d l |> verifyTrial 0.001 p
    printf "| %2i | %3i | %5.2f%% |\n"
        <| d
        <| l
        <| p * 100.0
for l in 8..128 do
    let d = seek l
    printf "| %2i | %3i | %5.2f%% |\n"
        <| d
        <| l
        <| (p d l) * 100.0
