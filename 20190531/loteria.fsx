#load "lib/math.fs"
#load "lib/random.fs"
#load "lib/simulation.fs"
open Math
open Random
open Simulation

let p n k =
    let rec p' i acc =
        let acc =
            acc
                + float k
                    * product (i - k) (i - 1)
                    * product (n - i - k) (n - i)
                    / product (n - 2 * k) n
        if i = k then acc
        else p' (i - 1) acc
    ((n - k) |> choose k) / (n |> choose k) * 2.0 * (p' (n - k) 0.0)

let loteria n k rnd =
    let player1 = rnd |> take n k
    let player2 = rnd |> take n k
    let cards = rnd |> take n n
    let rec loteria' (player1Matches, player1Count) (player2Matches, player2Count) cards =
        match cards with
        | [] -> failwith "a player must win before running out of cards"
        | card::cards ->
            let (player1Matches, player1Count) =
                if player1 |> List.contains card then
                    card::player1Matches, player1Count + 1
                else
                    player1Matches, player1Count
            let (player2Matches, player2Count) =
                if player2 |> List.contains card then
                    card::player2Matches, player2Count + 1
                else
                    player2Matches, player2Count
            if player1Count > 0 && player2Count > 0 then false
            elif player1Count = k || player2Count = k then true
            else cards |> loteria' (player1Matches, player1Count) (player2Matches, player2Count)
    cards |> loteria' ([], 0) ([], 0)

let n = 54
for k in [|1; 4; 9; 16; 25|] do
    let p = p n k
    loteria n k |> verifyTrial 0.005 p
    printf "p(%i, %i) = %e\n" n k p
