# [Can You Win The Lotería?](https://fivethirtyeight.com/features/can-you-win-the-loteria/)

# Solution
In order for one of the players to possibly lose the game without any cards called requires the two grids of size $`k`$ called to be not have any overlapping cards of a total possible $`n`$.  Thus, our starting probability is:
```math
\frac{{n - k} \choose k}{ n \choose k}
```

Then, given two non-overlapping grids the probability that all cards from one grid are called before the cards from the other grid is:
```math
\displaystyle
2 \cdot\frac{\displaystyle\sum_{i=k}^{n-k} {{n - 2 \cdot k} \choose {i - k}} \cdot (i - 1)! \cdot k \cdot (n - i)!}{n!}
```
that is, for either grid winning we need to count how many possible card pack permutations allow for the cards from one of the grid to occur before the other.  Thus, there are three groups of cards:
 - the winning $`k`$ cards,
 - the losing $`k`$ cards, and
 - all the other $`n - 2 \cdot k`$ cards.

However, the cards are drawn until there is a winner (however, the implicit order of the remaining cards is important).  The drawn cards will always end with a winning card.  Thus, the drawn cards are composed of winning cards and between $`0`$ and $`n - 2 \cdot k`$ other cards.

# Answer
p(54, 1) = 9.814815e-001
p(54, 4) = 2.080626e-002
p(54, 9) = 6.854655e-006
p(54, 16) = 3.507953e-012
p(54, 25) = 2.232517e-025
