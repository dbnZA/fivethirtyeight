#load "lib/math.fs"
#load "lib/simulation.fs"
open Math
open Simulation

let rec p n =
    let rec p' p i acc =
        match p with
        | [] -> 0.5 * acc
        | p_i::ptail -> p' ptail (i - 1) (acc + ((n - 1) |> choose i) * 2.0 ** -float (n - 1) * p_i)
    match n with
    | 0 -> [1.0]
    | _ ->
        let ptail = p (n - 1)
        (p' ptail (n - 1) 0.0)::ptail

let thanosClicks n rnd =
    let rec thanosClicks' i clicks =
        match i with
        | 0 -> clicks
        | _ ->
            let survivos =
                rnd
                    |> Seq.take (i - 1)
                    |> Seq.filter (fun i -> i >= 0.5)
                    |> Seq.length
            thanosClicks' survivos (clicks + 1)
    float (thanosClicks' n 0)

let pow b e =
    b ** e

let population = 7_500_000_000.0

printf "|  n |  p(n) = |      Survival |\n"
printf "|---:|--------:|--------------:|\n"
for thanoses in [1..50]@[63] do
    let p = List.head (p thanoses)
    let pN = population * p
    thanosClicks thanoses >> (~-) >> pow 2.0 |> verifyMean 0.005 p
    printf "| %2i | %6.3f%% | %13s |\n"
        <| thanoses
        <| p * 100.0
        <| pN.ToString("N0")
