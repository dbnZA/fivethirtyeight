# [How Many Earthlings Would Survive 63 Thanos Snaps?](https://fivethirtyeight.com/features/how-many-earthlings-would-survive-63-thanos-snaps/)
## Solution
After the first Thanos clicks his fingers there will be $`1/2`$ as many people remaining.  If there are any other Thanoses ($`n - 1`$) then those Thanoses will be randomly allocated, at probability $`1/2`$, to survive.  Thus, after the first click, the number of additional Thanoses that remain follow a Binomial distribution with $`p=1/2`$.

The next surviving Thanos then clicks his fingers, repeating the process above.

Let

```math
p(n) = \begin{cases}
\displaystyle 1 & n = 0 \\
\displaystyle \frac{1}{2} \sum_{i = 0}^{n - 1} b(n - 1, i) \cdot p(i) & n > 0
\end{cases}
```
where

```math
b(n, i) = {n - 1 \choose i} \cdot 2^{-(n - 1)} \text{ is the pmf of } B\left(n - 1, \frac{1}{2}\right) \\
i \text{ is the number of surviving Thanoses after the click for }n=i \\
p(n) \text{ is the probability of surviving the clicks from }n\text{ Thanoses}
```

## Answer
|  n |  p(n) = |      Survival |
|---:|--------:|--------------:|
|  1 | 50.000% | 3 750 000 000 |
|  2 | 37.500% | 2 812 500 000 |
|  3 | 29.688% | 2 226 562 500 |
|  4 | 24.512% | 1 838 378 906 |
|  5 | 20.883% | 1 566 238 403 |
|  6 | 18.208% | 1 365 605 593 |
|  7 | 16.152% | 1 211 386 481 |
|  8 | 14.518% | 1 088 855 786 |
|  9 | 13.186% |   988 965 387 |
| 10 | 12.078% |   905 881 330 |
| 11 | 11.142% |   835 663 694 |
| 12 | 10.341% |   775 539 423 |
| 13 |  9.647% |   723 487 849 |
| 14 |  9.040% |   677 994 812 |
| 15 |  8.505% |   637 900 304 |
| 16 |  8.031% |   602 299 856 |
| 17 |  7.606% |   570 478 078 |
| 18 |  7.225% |   541 862 270 |
| 19 |  6.880% |   515 989 112 |
| 20 |  6.566% |   492 480 190 |
| 21 |  6.280% |   471 023 707 |
| 22 |  6.018% |   451 360 618 |
| 23 |  5.777% |   433 274 014 |
| 24 |  5.554% |   416 580 911 |
| 25 |  5.348% |   401 125 849 |
| 26 |  5.157% |   386 775 874 |
| 27 |  4.979% |   373 416 563 |
| 28 |  4.813% |   360 948 869 |
| 29 |  4.657% |   349 286 581 |
| 30 |  4.511% |   338 354 286 |
| 31 |  4.374% |   328 085 707 |
| 32 |  4.246% |   318 422 343 |
| 33 |  4.124% |   309 312 353 |
| 34 |  4.009% |   300 709 628 |
| 35 |  3.901% |   292 573 015 |
| 36 |  3.798% |   284 865 673 |
| 37 |  3.701% |   277 554 519 |
| 38 |  3.608% |   270 609 766 |
| 39 |  3.520% |   264 004 523 |
| 40 |  3.436% |   257 714 457 |
| 41 |  3.356% |   251 717 496 |
| 42 |  3.280% |   245 993 575 |
| 43 |  3.207% |   240 524 417 |
| 44 |  3.137% |   235 293 339 |
| 45 |  3.070% |   230 285 085 |
| 46 |  3.006% |   225 485 675 |
| 47 |  2.945% |   220 882 278 |
| 48 |  2.886% |   216 463 095 |
| 49 |  2.830% |   212 217 261 |
| 50 |  2.775% |   208 134 751 |
| 63 |  2.220% |   166 492 281 |
