#load "lib/simulation.fs"
#load "lib/queue.fs"
open Simulation

[<Literal>]
let Pitches = 4

[<Literal>]
let HittingStreak = 57

let p b n =
    let g = 1.0 - (1.0 - b) ** float Pitches
    let g57 = g ** float HittingStreak
    let rec p' i pi =
        let pi1 = pi |> Queue.peek
        let pi, pi2 = pi |> Queue.dequeue
        let p = if i = HittingStreak then g57 else pi1 + (1.0 - pi2) * (1.0 - g) * g57
        if i = n then  p else p' (i + 1) (p |> Queue.enqueue pi)
    match n < HittingStreak with
    | true -> 0.0
    | false -> p' HittingStreak (Queue.init HittingStreak (fun _ -> 0.0))

let streak b n rnd =
    let g = 1.0 - (1.0 - b) ** float Pitches
    let lnG = g |> log
    let rec streak' n =
        if n < HittingStreak then false
        else
            let hittingStreak =
                rnd
                    |> Seq.head
                    |> log
                    |> ((/) >> (|>) lnG)
                    |> int
            if hittingStreak >= HittingStreak then true
            else streak' (n - hittingStreak - 1)
    streak' n

printf "|    n |     b |   p(n) |\n"
printf "|-----:|------:|-------:|\n"
for (n, b) in [3200, 0.200; 3200, 0.250; 3200, 0.300; 3200, 0.350; 3200, 0.400; 1600, 0.500] do
    let p = p b n
    streak b n |> verifyTrial 0.005 p
    printf "| %4i | %5.3f | %5.2f%% |\n"
        <| n
        <| b
        <| p * 100.0
