# [Can The Riddler Bros. Beat Joe DiMaggio’s Hitting Streak?](https://fivethirtyeight.com/features/can-the-riddler-bros-beat-joe-dimaggios-hitting-streak/)

## Solution

A batting average $`b`$ is the average hit rate per plate appearance and [a hitting streak is the number of consecutive official games in which a player appears and gets at least one base hit.][1]  Thus, the chance of a base hit per game $`g`$ is: $`g = 1 - (1 - b)^4`$.

The chance of beating a hitting streak of 56 is $`g^{57}`$.  Thus, assuming the hitting streak can start at any time we have:

```math
p(n) \approx (n - 57) \cdot g^{57}
```

However, this does not take into account the possibility of a hitting streak starting prior or post the designated hitting streak.  If we eliminate prior hitting streaks then we successfully avoid any double counting as post hitting streaks will not be replicated by a later designated hitting streak starting at that point.  Also, to ensure our designated hitting streak starts at the designate index we must ensure the previous game was not a hit.  Thus, we have:

```math
p(n) = \begin{cases}
\displaystyle 0 & n < 57 \\
\displaystyle g^{57} \left(1+(1 - g) \sum_{i=1}^{n - 57} (1 - p(i)) \right)& n \ge 57 \\
\end{cases}
```

This can be simplified to a recursive formula:

```math
p(n) = \begin{cases}
0 & n < 57> \\
g^{57} & n = 57 \\
p(n - 1) + g^{57} (1 - g) (1 - p(n - 57)) & n > 57
\end{cases}
```

## Answer
|    n |     b |   p(n) |
|-----:|------:|-------:|
| 3200 | 0.200 |  0.00% |
| 3200 | 0.250 |  0.00% |
| 3200 | 0.300 |  0.01% |
| 3200 | 0.350 |  0.76% |
| 3200 | 0.400 | 13.93% |
| 1600 | 0.500 | 93.35% |

[1]: https://en.wikipedia.org/wiki/Hitting_streak
