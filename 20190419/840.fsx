let factors n =
    let rec factors' i acc =
        match i with
        | 0 -> acc
        | _ ->
            let acc' = if n % i = 0 then i::acc else acc
            factors' (i - 1) acc'
    factors' (n / 2) [n]

let after840 =
    Seq.initInfinite id
        |> Seq.map (fun n -> n, n |> factors |> List.length)
        |> Seq.filter (snd >> (<) 32)
        |> Seq.head

printf "%A\n" after840
