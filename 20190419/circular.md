# [A concise circular conundrum](https://fivethirtyeight.com/features/what-comes-after-840-the-answer-may-surprise-you/)
Let

```math
p(n) = \frac{n}{2^{n-1}}
```

be the probability that $n$ points randomly placed on a circle's circumference will leave half the circle without any points.

## Proof
### Simplification
Since the points are placed on a circle and the circle can be rotated without changing the probabilities' of the points' placement one can simplify the solution by:
 1. Always "rotate" the circle so that a point is at 6 o'clock.
 2. Define the coordinates to be $`0`$ / $`1`$ at 12 o'clock and $`0.5`$ at 6 o'clock.
 3. "Unwrap" the circle to create a straight line between $`[0, 1]`$ (without concern of "wrap-around" due to step 1).

With the straight line, and one point at 0.5, then the problem can be restated as:
 - What is the probability that $`n - 1`$ points fall within a range of $`0.5`$ from each other.

### Ancillary proofs
Let $`L_n = \min(U_1,\dots,U_n)`$ be the smallest number from a set of $`n`$ independent $`U_i \sim U[0,1]`$, then

```math
\begin{aligned}
\mathbb{P}[L_n < x] &= 1 - \mathbb{P}[x < L_n] \\
&= 1 - \mathbb{P}[x < U_i \cap \dots \cap x < U_n] \\
&= 1 - \prod_{i=1}^n \mathbb{P}[x < U_i] \\
&= 1 - (1 - x)^n \\
\therefore p_{L_n}(x) &= n (1 - x)^{n-1}
\end{aligned}
```

Let $`\bar{U}_j = U_j[L_n, 1]`$ where $`j \in \{i|U_i \ne L_n\}`$ be the random $`n - 1`$ variables that are not the smallest.  These variables have a independent uniform distribution over $`[L_n, 1]`$.  Then,

```math
\prod_j \mathbb{P}[\bar{U}_j < y] = \left(\frac{y - L_n}{1 - L_n}\right)^{n-1}
```

### Solution
To calculate the probability that $`n - 1`$ points fall within a $`0.5`$ range there are two conditions:
 1. The smallest point $`L_{n-1}`$ is less than $0.5$ and all the other points $`\bar{U}_j`$ are within $`0.5`$ of $`L_{n-1}`$; and
 2. The smallest point $`L_{n-1}`$ is greater than $`0.5`$ (and thus all other points $`n-2`$ are within $`0.5`$).

Therefore:

```math
\begin{aligned}
p(n) &= \int_0^{0.5} p_{L_{n-1}}(x) \prod_{n-2}\mathbb{P}[\bar{U}_j < 0.5 + x] dx + \mathbb{P}[L_{n-1} > 0.5] \\
&= \int_0^{0.5} (n - 1)(1 - x)^{n - 2}  \left( \frac{0.5}{1 - x}\right)^{n - 2} dx + \frac{1}{2^{n - 1}} \\
&= \frac{n}{2^{n - 1}}
\end{aligned}
```

## Verification
|  N |   p(n) |
|---:|-------:|
|  1 | 100.0% |
|  2 | 100.0% |
|  3 |  75.0% |
|  4 |  50.0% |
|  5 |  31.3% |
|  6 |  18.8% |
|  7 |  10.9% |
|  8 |   6.3% |
|  9 |   3.5% |
| 10 |   2.0% |
| 11 |   1.1% |
| 12 |   0.6% |
| 13 |   0.3% |
| 14 |   0.2% |
| 15 |   0.1% |
| 16 |   0.0% |
| 17 |   0.0% |
| 18 |   0.0% |
| 19 |   0.0% |
| 20 |   0.0% |
