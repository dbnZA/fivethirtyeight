#load "lib/simulation.fs"
open Simulation

let p n =
    float n * 0.5 ** float (n - 1)

let bisectable n rnd =
    let circumference =
        rnd
            |> Seq.take (n - 1)
            |> Seq.sort
            |> (Seq.append >> (|>) [1.0])
    let gaps (maxGap, prevPoint) point =
        let gap = point - prevPoint
        (max maxGap gap, point)
    let gap =
        circumference
            |> Seq.fold gaps (0.0, 0.0)
            |> fst
    gap >= 0.5

printf "|  N |   p(n) |\n"
printf "|---:|-------:|\n"
for n in 1..20 do
    let p = p n
    bisectable n |> verifyTrial 0.005 p
    printf "| %2i | %5.1f%% |\n"
        <| n
        <| p * 100.0
