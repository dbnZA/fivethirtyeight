module Random

let take n k rnd =
    let rec take' k' (taken, orderedTaken) =
        if k' = k then taken
        else
            let rec adjust' orderedTaken choice =
                match orderedTaken with
                | [] -> [choice], choice
                | orderedChoice::orderedTaken ->
                    if choice < orderedChoice then choice::orderedChoice::orderedTaken, choice
                    else
                        let (orderedTaken, choice) = adjust' orderedTaken (choice + 1)
                        orderedChoice::orderedTaken, choice
            let (orderedTaken, choice) =
                (rnd |> Seq.head) * float (n - k')
                    |> int
                    |> adjust' orderedTaken
            take' (k' + 1) (choice::taken, orderedTaken)
    take' 0 ([], [])
