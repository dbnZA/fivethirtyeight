module Simulation
open System

let rnd =
    let random = Random()
    Seq.initInfinite (fun _ -> random.NextDouble())

[<Literal>]
let CI = 2.576

[<Literal>]
let MinSims = 1000

let simulate tolerate f =
    let rec simulate' i sumX sumXSqrd =
        let sampleVariance = (sumXSqrd - sumX ** 2.0 / float i) / float (i - 1)
        if i > MinSims && sqrt (sampleVariance / float i) * CI < tolerate then
            sumX / float i
        else
            let x = rnd |> f
            simulate' (i + 1) (sumX + x) (sumXSqrd + x ** 2.0)
    simulate' 0 0.0 0.0

let verifyMean tolerate mean f =
    let mean' = simulate tolerate f
    if abs (mean - mean') > tolerate then
        printf "error: f = %f; f' = %f" mean mean'

let verifyTrial tolerate p f =
    let power p = int (ceil (p * (1.0 - p) * (CI / tolerate) ** 2.0))
    let sims =
        [p + tolerate; p; p - tolerate]
            |> List.map power
            |> List.max
    let rec simulate' i sumX =
        if i = sims then
            let p' = float sumX / float i
            if abs (p - p') > tolerate then
                printf "error: p = %f%%; p' = %f%%; trials = %i \n"
                    <| p * 100.0
                    <| p' * 100.0
                    <| i
        else
            let sumX = sumX + (if rnd |> f then 1 else 0)
            simulate' (i + 1) sumX
    simulate' 0 0
