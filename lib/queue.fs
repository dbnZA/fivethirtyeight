module Queue

type Queue<'a> =
    | Queue of 'a list * 'a list

let init length initializer =
    Queue(List.init length initializer, [])

let enqueue queue item =
    match queue with
    | Queue(enqueue, dequeue) -> Queue(item::enqueue, dequeue)

let peek queue =
    match queue with
    | Queue([], []) -> failwith "empty queue"
    | Queue(item::_, _) -> item
    | Queue(_, dequeue) -> dequeue |> List.last

let dequeue queue =
    match queue with
    | Queue([], []) -> failwith "empty queue"
    | Queue(enqueue, item::dequeue) -> Queue(enqueue, dequeue), item
    | Queue(enqueue, []) ->
        let dequeue = enqueue |> List.rev
        Queue([], dequeue.Tail), dequeue.Head
