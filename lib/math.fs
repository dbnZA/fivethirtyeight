module Math

let product m n =
    if m > n then failwithf "product %i %i" m n
    let rec product' i acc =
        if i = m then acc
        else product' (i - 1) (acc * float i)
    product' n 1.0

let factorial = product 1

let choose k n =
    if k > n then failwithf "%i |> choose %i" n k
    if k = 0 || k = n then 1.0
    elif k < (n - k) then product (n - k) n / factorial k
    else product k n / factorial (n - k)

let rec combinations k n =
    if k = 0 then seq { yield [] }
    elif k = n then seq { yield List.init n id }
    else
        seq {
            for n in k .. n do
                for zs in combinations (k - 1) (n - 1) do
                    yield (n - 1)::zs
        }
